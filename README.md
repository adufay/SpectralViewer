# Spectral Viewer

The repository for this project has moved. Please find the latest version here:
- Website: https://mrf-devteam.gitlab.io/spectral-viewer
- Repository: https://gitlab.com/mrf-devteam/spectral-viewer